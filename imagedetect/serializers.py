from rest_framework import serializers
from .models import Post, PlantImage
import base64
from drf_extra_fields.fields import Base64ImageField

class PostSerializer(serializers.ModelSerializer):
	
	class Meta:
		model = Post
		fields = '__all__'


'''
class Base64ImageField(serializers.ImageField):

    def to_internal_value(self, data):
        from django.core.files.base import ContentFile
        import base64
        import six
        import uuid

        if isinstance(data, six.string_types):
            if 'data:' in data and ';base64,' in data:
                header, data = data.split(';base64,')

            try:
                decoded_file = base64.b64decode(data)
            except TypeError:
                self.fail('invalid_image')

            file_name = str(uuid.uuid4())[:12] # 12 characters are more than enough.
            file_extension = self.get_file_extension(file_name, decoded_file)
            complete_file_name = "%s.%s" % (file_name, file_extension, )
            data = ContentFile(decoded_file, name=complete_file_name)

        return super(Base64ImageField, self).to_internal_value(data)

    def get_file_extension(self, file_name, decoded_file):
        import imghdr

        extension = imghdr.what(file_name, decoded_file)
        extension = "jpg" if extension == "jpeg" else extension

        return extension
'''
class PhotoSerializer(serializers.ModelSerializer):
#    img = Base64ImageField(ma
	class Meta:
		model = PlantImage
		fields = '__all__'
	def create(self, validated_data):
		img=validated_data.pop('img')
		user=validated_data.pop('user')
		latitude=validated_data.pop('latitude')
		longitude=validated_data.pop('longitude')
		prediction=validated_data.pop('prediction')
		return PlantImage.objects.create(img=image, user=user, latitude=latitude, longitude=longitude, prediction=prediction)
