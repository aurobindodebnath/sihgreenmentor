from django.db import models
from api import settings
from django.contrib.auth.models import User

class Post(models.Model):
    title = models.CharField(max_length=120)
    img = models.ImageField(null=True, blank=True)
    content = models.TextField()
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)


    def __str__(self):
        return self.title
    
    class Meta:
        ordering = ["-timestamp", "-updated"]




class PlantImage(models.Model):
    user = models.ForeignKey(User)
    img = models.ImageField(blank=True)
    latitude = models.CharField(max_length= 120)
    longitude = models.FloatField()
    plant = models.CharField(max_length=120, default='')
    disease = models.CharField(max_length=120, default='')
    uploadedOn = models.DateTimeField(auto_now_add = True)
    probability = models.CharField(max_length=120, default='')
    def __str__(self):
        return str(self.id)


# Create your models here.
#class Detection(models.Model):
#	user = models.CharField(max_length=120)
#	latitude = models.CharField(max_length= 120)
#	longitude = models.CharField(max_length= 120)
#	prediction = models.IntegerField(blank=True, null=True)

#	def __str__(self):
#		return str(self.id)