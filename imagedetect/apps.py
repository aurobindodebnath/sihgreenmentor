from django.apps import AppConfig


class ImagedetectConfig(AppConfig):
    name = 'imagedetect'
