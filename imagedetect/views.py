import os
from django.conf import settings
import uuid
from django.shortcuts import render
from .models import Post, PlantImage
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import api_view
from .serializers import PostSerializer, PhotoSerializer
import json
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
import base64
import random, string
from pathlib import Path
from django.contrib.auth.models import User
from django.shortcuts import render
import subprocess
import tensorflow as tf
import numpy as np




@method_decorator(csrf_exempt, name='dispatch')
class postList(APIView):
	def get(self, request):
		posts = Post.objects.all()
		serializer = PostSerializer(posts, many=True)
		return Response(serializer.data)

	def post(self):
		pass

def id_generator(size=7, chars=string.ascii_uppercase + string.digits):
	return str(uuid.uuid4())


# Create your views here.
@api_view(['GET', 'POST', ])
@csrf_exempt
def uploadView(request):
	if request.method == 'GET':
		images = PlantImage.objects.all()
		serializer = PhotoSerializer(images, many=True)
		return Response(serializer.data)

	if request.method=='POST':
		data = request.data
		f = open('DATA.txt','w')
		f.write(str(type(data)))
		f.close()
		print("data is", data["latitude"], "longi", data["longitude"], data["user"])		
		image=base64.b64decode(data["img"])
		name = id_generator()
		print("congo we encoded")
		f= open(settings.BASE_DIR + "/media/"+name+".jpg", "wb")
		print("f is open")
		f.write(image)
		print("f is written")
		f.close()
		print("congo we wrote")
		user = User.objects.get(pk = int(data["user"]))
		obj = PlantImage.objects.create(user = user, img=name+".jpg", latitude=float(data["latitude"]), longitude=float(data["longitude"]))
		obj.save()
		print("congo object saved")
		os.chdir(settings.BASE_DIR+ "/tensorflow-for-poets-2/")
		os.system('pwd')
		x= subprocess.check_output(["python3", "-m", "scripts.label_image","--graph=tf_files/retrained_graph.pb","--image="+settings.BASE_DIR+"/media/" + name + '.jpg'])
		x=x.decode("utf-8")
		print("Hehehe")
		y=x.split("_")
		label=y[0].replace("'","")
		print(label)
		y[1]=y[1].replace("\n", "")
		y[2]=y[2].replace("\n", "")

		obj.plant = label
		obj.probability = y[2]
		obj.disease = y[1]
		obj.save()
		print("prediction saved")
		os.chdir(settings.BASE_DIR)
		return Response({"plant": label, "disease" : y[1], "probability": y[2]})


@api_view([ 'POST', ])
@csrf_exempt
def createUser(request):
	data = request.data
	username = data["username"]
	password = data["password"]
	try:
		user =User.objects.create(username=username)
		user.set_password(password)
		user.save()
		return Response({"userid": user.id})
	except Exception as E:
		return Response({"error" : str(E)})
	

def home(request):
	img = PlantImage.objects.all()
	d= {"points": img}
	return render(request, "home.html", d)

def seller(request):
	return render(request, "map.html",{})
