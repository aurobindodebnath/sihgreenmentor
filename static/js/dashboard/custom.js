$(document).ready(function() {
    $('#usersTable').DataTable({
        dom: 'Bfrtip',
        "bSort": false, //To keep the actual order sent by backend intact
        buttons: [{
            extend: 'csv',
            text: 'Download as CSV',
            exportOptions: {
                modifier: {
                    search: 'none'
                },
                columns: [ 0,1,2,3,4 ]
            }
        }
        ]
    });

} );

